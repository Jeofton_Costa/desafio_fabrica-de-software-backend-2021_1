# Desafio_Fabrica-de-Software-BackEnd-2021_1

Repositório para a recepção do projeto/teste para participação no projeto de extensão Fábrica de Software da UNIPÊ.
**DESAFIO - Entregar uma aplicação web dinâmica**
**PARTICIPANTES, ATENÇÃO ÀS REGRAS:**
**Regra 1** - Prazo para entrega do trabalho: sábado (20/03/2021) até as 23:59;
    Penalidade pelo não cumprimento do prazo: exclusão;
**Regra 2**- Entregar no mínimo o CRUD funcionando, ou seja, todas as operações de inserção(C), leitura(R), edição(U) e exclusão(D) de dados no banco, deve está funcionando sem bugs;
    Penalidade pela ausência da entrega: exclusão;
**Regra 3** - Serão avaliados:
    1 - Qualidade do código, ou seja, organização;
    2 - As funcionalidades adicionais ao que foi transferido no treinamento:
        1 - relacionamento entre entidade/tabelas (1:N, N:N e 1:1);
        2 - manipulação/utilização de arquivos estáticos (css, js, imagens, etc.);
        3 - campos de busca personalizadas;
        4 - manipulação dos atributos das entidades apenas pelo backend;
        5 - criação de sistema de login (caso você crie um sistema);
        6 - utilização do postgresql como banco padrão; 
    3 - Criativade/Originalidade da aplicação entregue;
    **4 - A entrega de mera cópia do projeto base, não assegura a continuidade no projeto de extensão fábrica de software, haja vista que viola a regra 2**;
**Regra 4** - A apresentação de prototipação/projeto/diagramas/roteiros, embora não seja essencial à entrega, poderá servir de critério de desempate para o ranking;
**Regra 5** - A interação dos participantes nos grupos/comunidades criadas para mútua ajuda, é permitida e incentivada. Contudo, membros da banca de avaliação não poderão agir diretamente na solução/correção de erros durante a etapa de desenvolvimento.
**Regra 6** - **O projeto deve ser entregue, unica e exclusivamente através deste repositório, com identificação do candidato através do nome completo e RGM.**
    1 - A entrega se fará por meio de branch especificamente criada pelo candidato, utilizando os conhecimentos adquiridos no treinamento de git/scrum.
    2 - **ATENÇÃO**: se o projeto for entregue na branch master, o candidato estará passível de exclusão do projeto;
**Regra 7** - Eventual violação as regras para entrega do projeto poderão se analisadas, não garantindo, contudo, que a penalidade pela respectiva violação não será aplicada.
